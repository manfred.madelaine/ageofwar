#ifndef JOUEUR_H
#define JOUEUR_H

#include "Base.hpp"
#include "Unite.hpp"
#include "Archer.hpp"
#include "Fantassin.hpp"
#include "Catapulte.hpp"
#include "Affichage.hpp"
#include "Terrain.hpp"
#include "stdafx.h"

class Joueur
{
    public:
        Joueur(Base& base);
        virtual ~Joueur();

        Base& getBase()             { return m_base; }
        void setBase(Base val)      { m_base = val; }

        void action1(Terrain &t);
        void action2(Terrain &t);
        void action3(Terrain &t);
        bool peutAcheter(Unite *u);
        bool peutAvancer(Unite &u);

        virtual void newUnite(Terrain &t)
                {cout<<"Erreur de definition du type de joueur"<<endl;}

    protected:
        Base &m_base;
};

#endif // JOUEUR_H
