#ifndef CATAPULTE_H
#define CATAPULTE_H

#include <iostream>
#include <string>
#include <sstream>

#include "Unite.hpp"
#include "stdafx.h"

using namespace std;

class Catapulte : public Unite
{
    public:
        Catapulte();
        virtual ~Catapulte();

        string toString() const;

        virtual bool peutAttaquer(Unite u);
        virtual bool peutAttaquerBase();
        virtual void attaquer(Unite &u, vector<Unite*>& tabTerrain, 
            vector<Unite*>& tabBase, int &sousBase, int &pv);
};

#endif // CATAPULTE_H
