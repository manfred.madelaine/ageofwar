#ifndef PARTIE_H
#define PARTIE_H

//#include <cstdlib.hpp>

#include "Unite.hpp"
#include "Fantassin.hpp"
#include "Catapulte.hpp"
#include "Archer.hpp"

#include "stdafx.h"
#include "Console.hpp"
#include "Terrain.hpp"

#include "Joueur.hpp"
#include "Player.hpp"
#include "Ia.hpp"

class Partie
{
    public:
        Partie();
        virtual ~Partie();

        Joueur* getJoueurA()            { return m_joueurA; }
        void setJoueurA(Joueur* val)    { m_joueurA = val; }
        Joueur* getJoueurB()            { return m_joueurB; }
        void setJoueurB(Joueur* val)    { m_joueurB = val; }
        Terrain& getTerrain()           { return m_terrain; }
        void setTerrain(Terrain& val)   { m_terrain = val; }
        int getTour()                   { return m_tour; }
        void setTour(int val)           {m_tour=val;}

        bool play();
        void menu();

        void mode();
        void modePlyr();
        void modeSpect();

        bool boucle();
        void update();
        bool nextTour();
        void nextPhase();
        void nextJoueur();
        bool tourJoueur(Joueur& j);

        bool endGame();
        bool init();
        bool attendreAction(string key);





    protected:

    private:
        Terrain m_terrain;

        Joueur *m_joueurA;
        Joueur *m_joueurB;
        int m_tour;
        int phase;
        bool isA;
};

#endif // PARTIE_H
