#include "Catapulte.hpp"

Catapulte::Catapulte(): Unite(PRIX_C, 0, PV_C, DPS_C, 2, 3)
{
    //ctor
}

Catapulte::~Catapulte()
{
    //dtor
}

string Catapulte::toString() const
{
    stringstream ss;
    ss << "-- Catapulte:" << endl << Unite::toString();
    return ss.str();
}


bool Catapulte::peutAttaquer(Unite u)
{
    if( ((((m_pos+m_poMin<=u.getPos()-1) && (m_pos+m_poMax+1>=u.getPos()-1)) || 
        ((m_pos-m_poMin-1>=u.getPos())   && (m_pos-m_poMax-2<=u.getPos())))) && 
        (campA!=u.isCampA())             && (m_pv>0))
    {
        okAction1=true;
        return true;
    }
    else return false;
}

bool Catapulte::peutAttaquerBase(){
    if( ((m_pos+1 == NB_CASES-1-m_poMax) &&  this->campA) ||
        ((m_pos-2 == 0+m_poMax)          && !this->campA) )
    {
        okAction1=true;
        return true;
    }
    else return false;
}

void Catapulte::attaquer(Unite &u, vector<Unite*>& tabTerrain, 
        vector<Unite*>& tabBase, int &sousBase, int &pv)
{
    if(peutAttaquer(u))
    {
        if((m_pos+m_poMin==u.getPos()-1)||(m_pos+m_poMax==u.getPos()-1))
        {
            u.setPv(u.getPv()-this->getPa());
            if(u.getPv()<=0)
            {
                sousBase+=(u.getPrix()/2);
                //Suppression des unit�s.
                for(unsigned int i=0;i<tabTerrain.size();i++)
                {
                    if(u.getPos()==tabTerrain[i]->getPos())
                    {
                        Affichage::degats(tabTerrain[i]->getPv());
                        tabTerrain.erase(tabTerrain.begin() + i);
                    }
                }
                for(unsigned int i = 0;i < tabBase.size(); i++)
                {
                    if(u.getPos() == tabBase[i]->getPos())
                    {
                        Affichage::degats(tabBase[i]->getPv());
                        tabBase.erase(tabBase.begin() + i);
                    }
                }
            }

            for(unsigned int i = 0;i < tabTerrain.size(); i++)
            {
                if(u.getPos() == tabTerrain[i]->getPos())
                {
                    tabTerrain[i]->setPv(tabTerrain[i]->getPv()-this->getPa());

                    if(tabTerrain[i]->getPv() <= 0)
                    {
                        sousBase += (tabTerrain[i]->getPrix()/2);
                        Affichage::degats(tabTerrain[i]->getPv());
                        tabTerrain.erase(tabTerrain.begin() + i);

                        for(unsigned int j=0;j<tabBase.size();j++)
                        {
                            if(tabTerrain[i]->getPos()==tabBase[j]->getPos())
                            {
                                Affichage::degats(tabBase[j]->getPv());
                                tabBase.erase(tabBase.begin() + j);
                            }
                        }
                    }
                }
            }
        }



        else if((m_pos+m_poMax+1 == u.getPos()-1))
        {
            u.setPv(u.getPv() - this->getPa());
            if(u.getPv() <= 0)
            {
                sousBase += (u.getPrix()/2);
                //Suppression des unit�s.
                for(unsigned int i = 0; i < tabTerrain.size(); i++)
                {
                    if(u.getPos() == tabTerrain[i]->getPos())
                    {
                        Affichage::degats(tabTerrain[i]->getPv());
                        tabTerrain.erase(tabTerrain.begin() + i);
                    }
                }
                for(unsigned int i = 0; i < tabBase.size(); i++)
                {
                    if(u.getPos() == tabBase[i]->getPos())
                    {
                        Affichage::degats(tabBase[i]->getPv());
                        tabBase.erase(tabBase.begin() + i);
                    }
                }
            }

            for(unsigned int i = 0; i < tabTerrain.size(); i++)
            {
                if(u.getPos()-2 == tabTerrain[i]->getPos())
                {
                    tabTerrain[i]->setPv(tabTerrain[i]->getPv()-this->getPa());

                    if(tabTerrain[i]->getPv() <= 0)
                    {
                        sousBase += (tabTerrain[i]->getPrix()/2);
                        Affichage::degats(tabTerrain[i]->getPv());
                        tabTerrain.erase(tabTerrain.begin() + i);

                        for(unsigned int j = 0; j < tabBase.size(); j++)
                        {
                            if(tabTerrain[i]->getPos() == tabBase[j]->getPos())
                            {
                                Affichage::degats(tabBase[j]->getPv());
                                tabBase.erase(tabBase.begin() + j);
                            }
                        }
                    }
                }
            }
        }


        if((m_pos-m_poMin-1 == u.getPos()) || (m_pos-m_poMax-1 == u.getPos()))
        {
            u.setPv(u.getPv() - this->getPa());
            if(u.getPv() <= 0){
                sousBase += (u.getPrix()/2);
                //Suppression des unit�s.
                for(unsigned int i = 0; i < tabTerrain.size(); i++)
                {
                    if(u.getPos() == tabTerrain[i]->getPos())
                    {
                        Affichage::degats(tabTerrain[i]->getPv());
                        tabTerrain.erase(tabTerrain.begin() + i);
                    }
                }
                for(unsigned int i = 0; i < tabBase.size(); i++)
                {
                    if(u.getPos() == tabBase[i]->getPos())
                    {
                        Affichage::degats(tabBase[i]->getPv());
                        tabBase.erase(tabBase.begin() + i);
                    }
                }
            }

            for(unsigned int i = 0; i < tabTerrain.size() ;i++)
            {
                if(u.getPos()-1 == tabTerrain[i]->getPos())
                {
                    tabTerrain[i]->setPv(tabTerrain[i]->getPv()-this->getPa());

                    if(tabTerrain[i]->getPv() <= 0)
                    {
                        sousBase += (tabTerrain[i]->getPrix()/2);
                        Affichage::degats(tabTerrain[i]->getPv());
                        tabTerrain.erase(tabTerrain.begin() + i);

                        for(unsigned int j = 0; j < tabBase.size(); j++)
                        {
                            if(tabTerrain[i]->getPos() == tabBase[j]->getPos())
                            {
                                Affichage::degats(tabBase[j]->getPv());
                                tabBase.erase(tabBase.begin() + j);
                            }
                        }
                    }
                }
            }
        }



        else if(/*(m_pos+m_poMax==u.getPos())||*/(m_pos-m_poMax-2==u.getPos()))
        {
            u.setPv(u.getPv()-this->getPa());
            if(u.getPv()<=0){
                sousBase+=(u.getPrix()/2);
                //Suppression des unit�s.
                for(unsigned int i=0;i<tabTerrain.size();i++)
                {
                    if(u.getPos()==tabTerrain[i]->getPos())
                    {
                        Affichage::degats(tabTerrain[i]->getPv());
                        tabTerrain.erase(tabTerrain.begin() + i);
                    }
                }
                for(unsigned int i=0;i<tabBase.size();i++)
                {
                    if(u.getPos()==tabBase[i]->getPos())
                    {
                        Affichage::degats(tabBase[i]->getPv());
                        tabBase.erase(tabBase.begin() + i);
                    }
                }
            }

            for(unsigned int i=0;i<tabTerrain.size();i++)
            {
                if(u.getPos()+1==tabTerrain[i]->getPos())
                {
                    tabTerrain[i]->setPv(tabTerrain[i]->getPv()-this->getPa());

                    if(tabTerrain[i]->getPv()<=0)
                    {
                        sousBase+=(tabTerrain[i]->getPrix()/2);
                        Affichage::degats(tabTerrain[i]->getPv());
                        tabTerrain.erase(tabTerrain.begin() + i);

                        for(unsigned int j=0;j<tabBase.size();j++)
                        {
                            if(tabTerrain[i]->getPos()==tabBase[j]->getPos())
                            {
                                Affichage::degats(tabBase[j]->getPv());
                                tabBase.erase(tabBase.begin() + j);
                            }
                        }
                    }
                }
            }
        }

    }
    else if(this->peutAttaquerBase()){
        pv-=this->getPa();
    }
}
