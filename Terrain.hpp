#ifndef TERRAIN_H
#define TERRAIN_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "Base.hpp"

using namespace std;

class Terrain
{
    public:
        Terrain(int nbCases);
        virtual ~Terrain();

        int getNbCases()                { return m_nbCases; }
        void setNbCases(int val)        { m_nbCases = val; }
        Base& getBaseA()                { return m_baseA; }
        void setBaseA(Base val)         { m_baseA = val; }
        Base& getBaseB()                { return m_baseB; }
        void setBaseB(Base val)         { m_baseB = val; }
        vector<Unite*>& getTab()        {return tab;}
        Unite&  getUniteTab(int val)    { return *tab[val]; }

        void newTour();
        void initTerrain();

    private:
        int m_nbCases;
        Base m_baseA;
        Base m_baseB;
        vector<Unite*> tab;

};

#endif // TERRAIN_H
