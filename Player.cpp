#include "Player.hpp"

Player::Player(Base& b) : Joueur(b)
{
    //ctor
}

Player::~Player()
{
    //dtor
}


void Player::newUnite(Terrain &t){
    bool posLibre=true;
    for(unsigned int i=0;i<m_base.getTab().size();i++){
        if( (m_base.getUniteTab(i).getPos() == 0  &&  m_base.getUniteTab(i).isCampA()) ||
            (m_base.getUniteTab(i).getPos() == 13 && !m_base.getUniteTab(i).isCampA()) ){
            posLibre=false;
        }
    }
    Affichage::acheter();

    string rep("");
    string enumeration[4];
    enumeration[0] = "1";
    enumeration[1] = "2";
    enumeration[2] = "3";
    enumeration[3] = "4";
    getline(cin,rep);

    while(Affichage::repIncorrecte(rep, enumeration, 4)){
        Affichage::acheter();
        getline(cin,rep);
    }

    if (rep.compare(enumeration[0]) ==0 && posLibre)
    {
        Fantassin *f= new Fantassin();
        if(peutAcheter(f)){
            if(&m_base==&t.getBaseB()){
                f->setCampA(false);
                f->setPos(NB_CASES);
            }
            cout <<"Ajout d'un fantassin dans Player"<<endl;
            m_base.ajouterUnite(f,t.getTab());
        }
        else cout<<"Vous n'avez pas assez de sous"<<endl;

    }
    else if(rep.compare(enumeration[1]) ==0 && posLibre)
    {
        Archer *a = new Archer();
        if(peutAcheter(a)){
            if(&m_base==&t.getBaseB()){
                a->setCampA(false);
                a->setPos(NB_CASES);
            }
            cout <<"Ajout d'un archer dans Player"<<endl;
            m_base.ajouterUnite(a,t.getTab());
        }
        else cout<<"Vous n'avez pas assez de sous"<<endl;
    }

    else if (rep.compare(enumeration[2]) ==0&&posLibre)
    {
        Catapulte *c = new Catapulte();
        if(peutAcheter(c)){
            if(&m_base==&t.getBaseB()){
                c->setCampA(false);
                c->setPos(NB_CASES);
            }
            cout <<"Ajout d'une catapulte dans Player"<<endl;
            m_base.ajouterUnite(c,t.getTab());
        }

        else cout<<"Vous n'avez pas assez de sous"<<endl;
    }
    else if(!posLibre)
        cout<<"La position pour creer une unite n'est pas libre"<<endl;
    else
        cout <<"Vous avez choisi de ne rien faire"<<endl;
}
