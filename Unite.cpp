#include "Unite.hpp"

Unite::Unite(int prix, int pos, int pv, int pa, int poMin, int poMax)
    : m_prix(prix), m_pos(pos), m_pv(pv), m_pa(pa), m_poMin(poMin), m_poMax(poMax)
{
    campA=true;
    okAction1=false;
}

Unite::~Unite()
{
    //dtor
}

string Unite::toString() const
{
    stringstream ss;
    ss << "PX: " << m_prix << "\tPOS: " << m_pos << "\nPV: " <<m_pv << 
    "\tPA: " << m_pa << "\tPOMIN: " << m_poMin<< "\tPOMAX: " << m_poMax;

    return ss.str();
}

bool Unite::peutAttaquer(Unite u){
    if( (((m_pos+m_poMin<=u.getPos()-1) && (m_pos+m_poMax>=u.getPos()-1)) || 
        ((m_pos-m_poMin-1>=u.getPos()) && (m_pos-m_poMax-1<=u.getPos()))) && 
        (campA!=u.isCampA()))
    {
        okAction1=true;
        return true;
    }
    else return false;
}

void Unite::action2(vector<Unite*>& tab)
{
    moveU(campA,tab);
}

void Unite::moveU(bool droite, vector<Unite*> &tab)
{
    //on va vers la droite
    bool caseSuivanteLibre=true;
    for(unsigned int i=0;i<tab.size();i++)
    {
        if(campA)
        {
            if(tab[i]->isCampA())
            {
                if(tab[i]->getPos()==m_pos+1)
                    caseSuivanteLibre=false;
            }
            else
            {
                if(tab[i]->getPos()-1==m_pos+1)
                    caseSuivanteLibre=false;
            }

        }
        else {
            if(tab[i]->isCampA())
            {
                if(tab[i]->getPos()==m_pos-2)
                    caseSuivanteLibre=false;
            }
            else
            {
                if(tab[i]->getPos()-1==m_pos-2)
                    caseSuivanteLibre=false;
            }
        }
    }

    if(caseSuivanteLibre)
    {
        if(droite&&(m_pos+m_poMax<NB_CASES-1))
            m_pos++;
        
        else if(!droite &&(m_pos-1-m_poMax>0))
            m_pos--;
    }

}

bool Unite::peutAttaquerBase(){
    if( ((m_pos   == NB_CASES-1-m_poMax) &&  this->campA) ||
        ((m_pos-1 == 0+m_poMax)          && !this->campA) )
    {
        okAction1=true;
        return true;
    }
    else return false;
}

void Unite::attaquer(Unite &u, vector<Unite*>& tabTerrain, 
                vector<Unite*>& tabBase, int &sousBase, int &pv)
{
    if(this->peutAttaquer(u))
    {
        u.setPv(u.getPv() - this->getPa());

        if(u.getPv() <= 0)
        {
            sousBase += (u.getPrix()/2);

            //Suppression des unit�s.
            for(unsigned int i = 0; i < tabTerrain.size(); i++)
            {
                if(u.getPos() == tabTerrain[i]->getPos())
                    tabTerrain.erase(tabTerrain.begin() + i);
            }
            for(unsigned int i = 0; i < tabBase.size(); i++)
            {
                if(u.getPos() == tabBase[i]->getPos())
                    tabBase.erase(tabBase.begin() + i);
            }
        }
    }
    else if(this->peutAttaquerBase())
        pv-=this->getPa();
}
