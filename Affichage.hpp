#ifndef AFFICHAGE_H
#define AFFICHAGE_H

#include <iostream>
#include <stdlib.h>

#include "stdafx.h"

using namespace std;

class Affichage
{
    public:
        Affichage();
        virtual ~Affichage();

        static void tour(int tour);
        static void phase1();
        static void phase2();
        static void phase3();
        static void acheter();

        static void degats(int pvPerdus);

        static void moveIncorecte();
        static void toucheIncorecte();
        static void reponseIncorecte();
        static void restartImpossible();
        static void fonctIndisponible();
        static bool repIncorrecte(string rep, string enumeration[], int l);

        static void pasDeVainceur();
        static void vainqueur(bool campA);
};

#endif // AFFICHAGE_H
