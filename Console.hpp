
#ifndef CONSOLE_H
#define CONSOLE_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <time.h>
#include <string>

#include "Affichage.hpp"
#include "Fantassin.hpp"
#include "Catapulte.hpp"
#include "Terrain.hpp"
#include "Archer.hpp"
#include "Unite.hpp"
#include "stdafx.h"

using namespace std;

class Console
{
    public:
        Console();
        virtual ~Console();

        static void menu();
        static void mode();
        static void intiMap();
        static void modePlayer();
        static void ecranAccueil();
        static void modeSpectateur();

        static void centrerTexte(string s, int posy);
        static void afficherQuestion(vector<string> blocQuestion);
        static void afficherText(string Result, int posX, int posY);
        static void affTextBox(vector<string> me, int posx, int posy);
        static void lireFichier(string nomFichier, int posX, int posY);


        static void clearMap();
        static string pauseConsole();
        static void nettoyerConsole();
        static void clearZ(int x, int y, int dx, int dy);

        static bool Move(int v, int m);
        static bool deplacement(string key);
        static void positionnerCuseur(int posx, int posy);

        static void update();
        static void retourEcran();
        static void updateQestion(bool modespec);
        static void updateUnites(vector<Unite*> tab);
        static void UpdateInfoBase(Base &b, bool isA);
        static void updateTerrain(Terrain &t, int tour,  bool joueurA, 
                                              int phase, bool modespec);

        static void controlKey();
        static void affTerrain();
        static bool addUnite(Unite *u);
        static vector<string> selectSkin(int skin);
        static bool drawUnite(Unite *u, vector<string> v, int posX);
        static void drawInfoBulle(vector<char> &text, int posx, int posy);
        static void infoPartie(int tour, int nbMob, bool joueurA, int phase);
        static void drawBox(char contour_l, char contour_h, char Fill, int l, 
                                                     int h, int posX, int posY);

        static void addGold();
        static void clignoter(int nb_clign, unsigned int microseconds, 
                                int posx, int posy, string old, string new_);

        static string intToString(int x);
};
extern char map[MAX_Y][MAX_X];
extern int x;
extern int y;

#endif // CONSOLE_H
