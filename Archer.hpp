#ifndef ARCHER_H
#define ARCHER_H

#include <iostream>
#include <string>
#include <sstream>

#include "Unite.hpp"
#include "stdafx.h"

using namespace std;

class Archer : public Unite
{
    public:
        Archer();
        virtual ~Archer();

        string toString() const;
};

#endif // ARCHER_H
