#ifndef BASE_H
#define BASE_H

#include <iostream>
#include<vector>
#include "Unite.hpp"

using namespace std;

class Base
{
    public:
        Base(int gold);
        virtual ~Base();

        int& getPv()                { return m_pv; }
        void setPv(int val)         { m_pv = val; }
        int& getGold()              { return m_gold; }
        void setGold(int val)       { m_gold = val; }
        vector<Unite*>& getTab()    { return tab; }
        Unite& getUniteTab(int val) { return *tab[val]; }

        void addGold(int gold);
        void removeGold(int gold);
        void ajouterUnite(Unite *u, vector<Unite*> &tab);

    private:
        int m_pv;
        int m_gold;
        vector<Unite*> tab;
};

#endif // BASE_H
