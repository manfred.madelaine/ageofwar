#include "Terrain.hpp"

Terrain::Terrain(int nbCases): m_nbCases(nbCases), m_baseA(GOLD_INIT), m_baseB(GOLD_INIT)
{

}

Terrain::~Terrain()
{
    //dtor
}

void Terrain::initTerrain()
{
    m_baseA.setPv(PV_BASE);
    m_baseB.setPv(PV_BASE);
    m_baseA.setGold(GOLD_INIT);
    m_baseB.setGold(GOLD_INIT);
    tab.clear();
}

void Terrain::newTour()
{
    m_baseA.addGold(GOLD_TOUR);
    m_baseB.addGold(GOLD_TOUR);
}
