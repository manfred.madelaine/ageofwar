#include "Partie.hpp"

Partie::Partie() :
    m_terrain(NB_CASES), m_joueurA(NULL), m_joueurB(NULL), m_tour(0), phase(), isA(true)
{

}

Partie::~Partie()
{
    //dtor
}

bool gamerunning = false;
bool menuB = true;
bool modeB = false;
bool modeSpec = true;


bool Partie::play()
{
    Console::ecranAccueil();
    while(menuB || gamerunning)
    {
        menu();
        mode();
        if( gamerunning)
            boucle();        
    }

    if(m_terrain.getBaseA().getPv() == m_terrain.getBaseB().getPv())
        Affichage::pasDeVainceur();
    else 
        Affichage::vainqueur(m_terrain.getBaseB().getPv() < 1);
    //si B < 1 pdv alors true affiche B vainqueur, si non c'est A 
    return true;
}

void Partie::modeSpect()
{
    Ia* ia1= new Ia(m_terrain.getBaseA());
    Ia* ia2= new Ia(m_terrain.getBaseB());
    m_joueurA = ia1;
    m_joueurB = ia2;
    modeSpec = true;
}
void Partie::modePlyr()
{
    Player* p= new Player(m_terrain.getBaseA());
    Ia* ia= new Ia(m_terrain.getBaseB());
    m_joueurA = p;
    m_joueurB = ia;
    modeSpec = false;
}

bool Partie::endGame()
{
    if(m_terrain.getBaseA().getPv() < 1 || m_terrain.getBaseB().getPv() < 1)
    {
        menuB = false;
        gamerunning = false;
        return true;
    }
    return false;
}


/** \brief
 *
 * \param
 * \param
 * \return true si la partie peut continuer, false si non
 *
 */
bool Partie::tourJoueur(Joueur& j)
{
    j.action1(m_terrain);
    update();

    if(endGame())
        return gamerunning;

    j.action2(m_terrain);
    update();

    j.action3(m_terrain);
    update();    

    if(endGame())
        return gamerunning;
    
    for(unsigned int i = 0; i < m_terrain.getTab().size(); i++)
        m_terrain.getUniteTab(i).setOkAction1(false);

    j.newUnite(m_terrain);
    update();

    return gamerunning;
}

void Partie::update()
{
    nextPhase();
    Console::updateTerrain(m_terrain, m_tour, isA, phase, modeSpec);
}

bool Partie::nextTour()
{
    if(m_tour < MAX_TOUR)
    {
        m_tour++;
        
        m_terrain.newTour();
        return true;
    }
    return false;
}


void Partie::nextPhase()
{
    phase++;
    if(phase > 4)
    {
        phase=1;
        nextJoueur();
    }
}

void Partie::nextJoueur()
{
    isA = !isA;
}


/** \brief enchaine les tours de jeu  d'une partie jusqu'a ce que le jeu se termine.
 *
 * \return true si l'un des joueurs n'a plus de base,
 *          false si plus aucun joueur ne peut joueur.
 */
bool Partie::boucle()
{
    Console::intiMap();
    while (nextTour())
    {
        if (!tourJoueur(*m_joueurA) || !tourJoueur(*m_joueurB))
            return true;
    }
    gamerunning = false;
    return false;
}

void Partie::menu()
{
    Console::menu();
    while (menuB)
    {
        Console::nettoyerConsole();
        Console::affTerrain();
        while( !attendreAction(Console::pauseConsole()) ){}
    }
}

void Partie::mode()
{
    Console::mode();

    while (modeB)
    {
        Console::nettoyerConsole();
        Console::affTerrain();
        while( !attendreAction(Console::pauseConsole()) ){}
    }
}


bool Partie::attendreAction(string key)
{
    if( key.compare(K_UP)    == 0 || key.compare(K_DOWN) == 0 ||
        key.compare(K_RIGHT) == 0 || key.compare(K_LEFT) == 0 )
    {
        return Console::deplacement(key);
    }

    //gestion de la partie
    else if(key.compare(KEY_R) == 0)
    {
        if(gamerunning)
       { 
            m_terrain.initTerrain();
            Console::intiMap();
            cout<< "restart"<<endl;
        }
        else
        {
            Affichage::restartImpossible();
            return false;
        }
    }
    //retour menu principal
    else if(key.compare(KEY_M) == 0)
    {
        menuB = true;
        modeB = false;
        gamerunning = false;
        cout<< "back to menu."<<endl;
    }
    else if(key.compare(KEY_W) == 0)
    {
        cout<< "A bientot!"<<endl;
        exit(EXIT_SUCCESS);
    }
    else if (key.compare(KEY_P) ==0)
    {
        Console::intiMap();
        menuB = false;
        modeB = true;
    }

    //choix du joueur: 1ere option
    else if(key.compare("1") ==0) 
    {
        if(modeB)
        {
            cout<< "IA vs IA"<<endl;
            modeB = false;
            gamerunning = true;
            modeSpec= true;
            modeSpect();
            //boite de dialogue

        } 
        else{
            Affichage::reponseIncorecte();
            return false;
        }
    }
    //choix du joueur: 2eme option
    else if(key.compare("2") ==0) 
    {
        if(modeB)
        {
            cout<< "Player vs IA"<<endl;
            modeB = false;
            gamerunning = true;
            modeSpec = false;
            modePlyr();
            //boite de dialogue
        } 
        else{
            Affichage::reponseIncorecte();
            return false;
        }
    }
    //choix du joueur: 3eme option
    else if(key.compare("3") ==0) 
    {
        if(modeB)
        {
            cout<< "retour menu"<<endl;
            modeB = false;
            menuB = true;
            gamerunning = false;
        } 
        else{
            Affichage::reponseIncorecte();
            return false;
        }
    }

    else if(key.size() == 0) {/*rien*/}
    else{
        Affichage::fonctIndisponible();
        return false;
    }
    return true;
}
