#include "Fantassin.hpp"

Fantassin::Fantassin(): Unite(PRIX_F, 0, PV_F, DPS_F, 1, 1), m_superSoldat(false)
{
    //ctor(10, 0, 10, 4, 1)
}

Fantassin::~Fantassin()
{
    //dtor
}

string Fantassin::toString() const
{
    stringstream ss;
    ss << "-- Fantassin:" << endl << Unite::toString();
    return ss.str();
}

void Fantassin::attaquer(Unite &u, vector<Unite*>& tabTerrain, 
            vector<Unite*>& tabBase, int &sousBase, int &pv)
{
    if(peutAttaquer(u))
    {
        u.setPv(u.getPv() - this->getPa());
        if(u.getPv() <= 0 )
        {
            sousBase += (u.getPrix()/2);
            if(u.getPrix() == PRIX_F)
                m_superSoldat = true;
            
            //Suppression des unit�s.
            for(unsigned int i = 0; i < tabTerrain.size(); i++)
            {
                if(u.getPos() == tabTerrain[i]->getPos())
                    tabTerrain.erase(tabTerrain.begin() + i);
            }
            for(unsigned int i = 0; i < tabBase.size(); i++)
            {
                if(u.getPos() == tabBase[i]->getPos())
                    tabBase.erase(tabBase.begin() + i);
            }
        }
    }
    else if(this->peutAttaquerBase())
        pv-=this->getPa();
    
}
