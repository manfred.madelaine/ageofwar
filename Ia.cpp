#include "Ia.hpp"

Ia::Ia(Base& b) : Joueur(b)
{
    //ctor
}

Ia::~Ia()
{
    //dtor
}

void Ia::newUnite(Terrain &t){
    bool posLibre=true;
    for(unsigned int i=0;i<m_base.getTab().size();i++){
        if((m_base.getUniteTab(i).getPos() == 0 && m_base.getUniteTab(i).isCampA())||
            (m_base.getUniteTab(i).getPos()==13 && !m_base.getUniteTab(i).isCampA()))
        {
            posLibre=false;
        }
    }
    if(m_base.getGold() >= PRIX_C && posLibre){
        cout<<"Ajout d'une catapulte dans Ia"<<endl;
        Catapulte *c = new Catapulte();
        if(&m_base == &t.getBaseB()){
            c->setCampA(false);
            c->setPos(NB_CASES);
        }
        m_base.ajouterUnite(c,t.getTab());
    }
    else if(m_base.getGold() >= PRIX_A && posLibre){
        cout<<"Ajout d'un archer dans Ia"<<endl;
        Archer *a= new Archer();
        if(&m_base==&t.getBaseB()){
            a->setCampA(false);
            a->setPos(NB_CASES);
        }
        m_base.ajouterUnite(a,t.getTab());
        //t.ajouterUnite(a);
    }
    else if(m_base.getGold() >= PRIX_F &&posLibre){
        cout<<"Ajout d'un fantassin dans Ia"<<endl;
        Fantassin *f = new Fantassin();
        if(&m_base == &t.getBaseB()){
            f->setCampA(false);
            f->setPos(NB_CASES);
        }
        m_base.ajouterUnite(f,t.getTab());
        //t.ajouterUnite(f);
    }
    else cout<<"Vous n'avez pas assez de sous"<<endl;
}
