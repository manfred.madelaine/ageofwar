#include "Archer.hpp"

Archer::Archer(): Unite(PRIX_A, 0, PV_A, DPS_A, 1, 3)
{
    //ctor
}

Archer::~Archer()
{
    //dtor
}

string Archer::toString() const
{
    stringstream ss;
    ss << "-- Archer:" << endl << Unite::toString();
    return ss.str();
}
