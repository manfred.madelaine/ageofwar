#ifndef PLAYER_H
#define PLAYER_H

#include "Joueur.hpp"
#include "stdafx.h"

class Player : public Joueur
{
    public:
        Player(Base& b);
        virtual ~Player();

        virtual void newUnite(Terrain &t);
};

#endif // PLAYER_H
