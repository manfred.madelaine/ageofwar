#ifndef FANTASSIN_H
#define FANTASSIN_H

#include <iostream>
#include <string>
#include <sstream>

#include "Unite.hpp"
#include "stdafx.h"

using namespace std;

class Fantassin : public Unite
{
    public:
        Fantassin();
        virtual ~Fantassin();

        string toString() const;
        void setSuper(bool v)   {m_superSoldat = v; }

        //redefinition
        virtual bool isSuper () {return m_superSoldat; }
        virtual void attaquer(Unite &u, vector<Unite*>& tabTerrain, 
                vector<Unite*>& tabBase, int &sousBase, int &pv);
        virtual int monSkin()
        {
            if(m_superSoldat) return m_prix+1;
            else return m_prix;
        }

    private:
        bool m_superSoldat;
};

#endif // FANTASSIN_H
