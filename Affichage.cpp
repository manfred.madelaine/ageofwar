#include "Affichage.hpp"

Affichage::Affichage()
{
    //ctor
}

Affichage::~Affichage()
{
    //dtor
}



void Affichage::tour(int i)
{
    cout << "Tour " << i << endl;
}

void Affichage::phase1()
{
    cout << "Phase 1:" << endl;
}

void Affichage::phase2()
{
    cout << "Phase 2:" << endl;
}

void Affichage::phase3()
{
    cout << "Phase 3:" << endl;
}

void Affichage::acheter()
{
    cout << "Quelle unite voulez-vous acheter ? (1-4)" <<
    "\n>> ";
}

void Affichage::degats(int pvPerdus)
{
    cout << "L'unite a perdu " << pvPerdus << " points de vie!" << endl;
}


/** \brief Verifie que la reponse fait bien partie des reponses autorisees
 *
 * \param rep : reponse a controler
 * \param enumeration[] : tableau des reponses autorise
 * \param l : longueur du tableau enumeration

 * \return true si la reponse est incorrecte (non comprise dans le tableau),
 *          false sinon.
 */
bool Affichage::repIncorrecte(string rep, string enumeration[], int l)
{
    for(int i = 0; i < l; i++)
    {
        if (enumeration[i]==rep)
            return false;
    }
    reponseIncorecte();
    return true;
}

void Affichage::fonctIndisponible()
{
    cout << "Cette fonctionnalite n'a pas encore ete implementee " <<
                "malheureusement." << endl;
}
void Affichage::reponseIncorecte()
{
    cout << "Votre reponse est incorrecte, veuillez rentrer un " <<
                "reponse valide." << endl;
}

void Affichage::moveIncorecte()
{
    cout << "on ne passe pas !"<<endl;
}

void Affichage::toucheIncorecte()
{
    cout << "La touche n'est pas reconnue." << endl;
}

void Affichage::restartImpossible()
{
    cout << "Impossible de recharger, il n'y a pas de partie en cours."
    <<endl;
}


void Affichage::pasDeVainceur()
{
    cout << "Il n'y a pas eu de vainqueur lors de cette partie." << endl;
}

void Affichage::vainqueur(bool campA){
    cout<<endl<<endl;
    if(campA){
        cout<<"Bravo le joueur A a gagne la partie"<<endl;
    }
    else cout<<"Bravo le joueur B a gagne la partie"<<endl;
    exit(EXIT_SUCCESS);
}
