#include "Joueur.hpp"

Joueur::Joueur(Base& b) : m_base(b)
{
    //ctor
}

Joueur::~Joueur()
{
    //dtor
}


void Joueur::action1(Terrain &t)
{
    Affichage::phase1();
    for(unsigned int i=0;i<m_base.getTab().size();i++){
            for(unsigned int j=0;j<t.getTab().size();j++){
                if(!m_base.getUniteTab(i).getOkAction1()){
                    if(m_base.getUniteTab(i).isCampA()){
                        m_base.getUniteTab(i).attaquer(t.getUniteTab(j), 
                            t.getTab(), t.getBaseB().getTab(),m_base.getGold(),
                            t.getBaseB().getPv());
                    }
                    else {
                        m_base.getUniteTab(i).attaquer(t.getUniteTab(j), 
                            t.getTab(), t.getBaseA().getTab(),m_base.getGold(),
                            t.getBaseA().getPv());
                    }
                }
            }
    }

}

void Joueur::action2(Terrain &t)
{
    Affichage::phase2();
    for(unsigned int i=0;i<m_base.getTab().size();i++){
        if(m_base.getUniteTab(i).getPrix()!=PRIX_C){
            m_base.getUniteTab(i).action2(t.getTab());
        }
    }
}

bool Joueur::peutAvancer(Unite &u)
{
    for(unsigned int i=0;i<m_base.getTab().size();i++){
        if(m_base.getUniteTab(i).isCampA() && 
            u.getPos()+1 == m_base.getUniteTab(i).getPos() ){
            return false;
        }
        else if(!m_base.getUniteTab(i).isCampA() && 
            u.getPos() -1 == m_base.getUniteTab(i).getPos() ){
            return false;
        }
    }
    return true;
}

void Joueur::action3(Terrain &t)
{
    Affichage::phase3();
    for(unsigned int i=0;i<m_base.getTab().size();i++){
                //Si c'est un super-soldat
            if(m_base.getUniteTab(i).getPrix()==PRIX_F){
                if(m_base.getUniteTab(i).isSuper()){
                    for(unsigned int j=0;j<t.getTab().size();j++){
                        if(!m_base.getUniteTab(i).getOkAction1()){
                            if(m_base.getUniteTab(i).isCampA()){
                                m_base.getUniteTab(i).attaquer(t.getUniteTab(j),
                                    t.getTab(), t.getBaseB().getTab(),
                                    m_base.getGold(),t.getBaseB().getPv());
                            }
                            else 
                                m_base.getUniteTab(i).attaquer(t.getUniteTab(j),
                                    t.getTab(), t.getBaseA().getTab(),
                                    m_base.getGold(),t.getBaseA().getPv());
                        }
                    }
                }
                //Sinon si c'est un fantassin qui n'as pas jouer au 1er tour
                else if(!m_base.getUniteTab(i).getOkAction1()){
                    for(unsigned int j=0;j<t.getTab().size();j++){
                        if(!m_base.getUniteTab(i).getOkAction1()){
                             if(m_base.getUniteTab(i).isCampA()){
                                m_base.getUniteTab(i).attaquer(t.getUniteTab(j), 
                                    t.getTab(), t.getBaseB().getTab(),
                                    m_base.getGold(),t.getBaseB().getPv());
                            }
                            else 
                                m_base.getUniteTab(i).attaquer(t.getUniteTab(j), 
                                    t.getTab(), t.getBaseA().getTab(),
                                    m_base.getGold(),t.getBaseA().getPv());
                        }
                    }
                }
            }
            //Si cest une catapulte qui n a pas jouer au premier tour
            else if((m_base.getUniteTab(i).getPrix()==PRIX_C) && 
                (!m_base.getUniteTab(i).getOkAction1())){
                m_base.getUniteTab(i).action2(t.getTab());
            }

    }
}

bool Joueur::peutAcheter(Unite *u)
{
    return (u->getPrix() <= m_base.getGold());
}
