#ifndef UNITE_H
#define UNITE_H

#include <iostream>
#include <string>
#include <sstream>
#include<vector>
#include "Affichage.hpp"
#include "stdafx.h"

using namespace std;

class Unite
{
    public:
        Unite(int prix, int pos, int pv, int pa, int poMin, int poMax);
        virtual ~Unite();

        int getPrix()               { return m_prix; }
        void setPrix(int val)       { m_prix = val; }
        int getPos()                { return m_pos; }
        void setPos(int val)        { m_pos = val; }
        int getPv()                 { return m_pv; }
        void setPv(int val)         { m_pv = val; }
        int getPa()                 { return m_pa; }
        void setpa(int val)         { m_pa = val; }
        int getPoMin()              { return m_poMin; }
        void setPoMin(int val)      { m_poMin = val; }
        int getPoMax()              { return m_poMax; }
        void setPoMax(int val)      { m_poMax = val; }
        bool isCampA()              { return campA; }
        void setCampA(bool val)     { campA = val; }
        bool getOkAction1()         { return okAction1; }
        void setOkAction1(bool val) { okAction1=val; }

        //methode susceptible d'etre redefinie par ses classes derives
        virtual bool isSuper ()     { return true; }
        virtual int monSkin()       { return m_prix; }

        virtual bool peutAttaquerBase();
        virtual bool peutAttaquer(Unite u);
        virtual void attaquer(Unite &u, vector<Unite*>& tabTerrain, 
                        vector<Unite*>& tabBase, int &sousBase, int &pv);

        void moveU(bool droite, vector<Unite*>& tab);
        void action2(vector<Unite*>& tab);
        string toString() const;

    protected:
        int m_prix;
        int m_pos;
        int m_pv;
        int m_pa;
        int m_poMin;
        int m_poMax;
        bool campA;
        bool okAction1;
};

#endif // UNITE_H
